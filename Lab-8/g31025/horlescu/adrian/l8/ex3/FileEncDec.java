package ex3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class FileEncDec {

    public FileEncDec() {
    }

    public void EncryptFile(String encryptedFileName, String decryptedFileName){
        try {
            FileInputStream fis = new FileInputStream(decryptedFileName);
            FileWriter fw = new FileWriter(encryptedFileName);
            String newline = "";
            char current;
            while (fis.available() >0){
                current = (char)fis.read();
                current--;
                newline = newline + current;
            }
            fw.append(newline);
            fis.close();
            if (fw!=null){
                fw.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void DecryptFile(String encryptedFileName, String decryptedFileName) {
        try {
            FileInputStream fis = new FileInputStream(encryptedFileName);
            FileWriter fw = new FileWriter(decryptedFileName);
            String newline = "";
            char current;
            while (fis.available() > 0) {
                current = (char) fis.read();
                current++;
                newline = newline + current;
            }
            fw.append(newline);
            fis.close();
            if (fw!=null){
                fw.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

