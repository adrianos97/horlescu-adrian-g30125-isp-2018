package ex1;

class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, CofeeMakerException{
        if(c.getNocof()>5)
            throw new CofeeMakerException(c.getNocof(),"Too many cofees");
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");

        System.out.println("Drink cofee:"+c);
    }
}
