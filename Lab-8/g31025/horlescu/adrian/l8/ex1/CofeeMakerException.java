package ex1;

public class CofeeMakerException extends Exception{
    int ts;
    public CofeeMakerException(int ts,String msg) {
        super(msg);
        this.ts = ts;
    }

    int getNoC(){
        return ts;
    }
}