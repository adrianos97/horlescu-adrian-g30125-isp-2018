package ex2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader {

    String file_name;


    public int countCharacter(String file_name, char c){
        try {
            int count = 0;
            FileInputStream file = new FileInputStream(file_name);
            char current;
            while (file.available() > 0){
                current = (char) file.read();
                if (current == c)
                    count++;
            }
            return count;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("This file cannot be found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
