package ex3;

class Counter1_2 extends Thread{
    Thread t;
    String name;
    Counter1_2(String name, Thread t){
        this.name=name;
        this.t=t;
    }

    public void run() {
        System.out.println("Contorul " + getName() + " este on!");
        if (t==null) {
        try {
            for (int i = 0; i <= 100; i++) {
                System.out.println(getName() + " i = " + i);
                try {
                    Thread.sleep((int) (Math.random() * 250));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " job finalised.");
        }catch(Exception e){e.printStackTrace();}
    }
    else {
            try {
                t.join();
                for (int i = 101; i <= 200; i++) {
                    System.out.println(getName() + " i = " + i);
                    try {
                        Thread.sleep((int) (Math.random() * 250));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(getName() + " job finalised.");
            }catch(Exception e){e.printStackTrace();}
        }
    }
    public static void main(String[] args)
    {
        Counter1_2 c1 = new Counter1_2("Counter 1",null);
        Counter1_2 c2 = new Counter1_2("Counter 2",c1);
        c1.start();
        c2.start();
    }
}
