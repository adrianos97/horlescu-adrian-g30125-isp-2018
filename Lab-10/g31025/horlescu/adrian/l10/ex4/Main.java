package ex4;

import java.util.Random;

    public class Main {

        public static void main(String[] args) {

            int[][] space = new int[10][10];

            for (int i = 0; i<10; i++){
                for (int j = 0; j<10; j++){
                    space[i][j] = 0;
                }
            }

            Random r = new Random(4);
            Robot r1 = new Robot(space, 3,5);
            Robot r2 = new Robot(space, 2,4);

            r1.start();
            r2.start();

        }
    }
