package ex5;
import org.junit.Test;

import static org.junit.Assert.	assertTrue;
import static org.junit.Assert.	assertEquals;

public class TestBook
{
    @Test
    public void shouldGetName()
    {
        Author a = new Author("Adrian","adrian@gmail.com","m");
        Book b = new Book("Tema", a, 100, 5);
        String n1 = "Tema";
        assertTrue(n1.equals(b.getName()));
    }
    @Test
    public void shouldGetAuthor()
    {
        Author a = new Author("Adrian","adrian@gmail.com","m");
        Book b = new Book("Tema", a, 100, 5);
        assertTrue(a.equals(b.getAuthor()));
    }
}
