package ex3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCircle
{
    @Test
    public void shouldGetRadius()
    {
        Circle c = new Circle(1.0);
        assertEquals(1.0,c.getRadius(),1);
    }
    @Test
    public void shouldGetArea()
    {
        Circle c = new Circle(1.0);
        assertEquals(9.8596,c.getRadius(),10);
    }
}
