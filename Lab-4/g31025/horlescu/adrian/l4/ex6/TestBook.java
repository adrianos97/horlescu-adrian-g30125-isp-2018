package ex6;



public class TestBook
{
    public static void main(String [] args)
    {
        Author[] autori = new Author[10];
        autori[0] = new Author("Adrian","adrian@gmail.com","m");
        autori[1] = new Author("Marian","marian@gmail.com","m");
        Book b = new Book("Tema", autori, 100, 5);
        System.out.println(b.toString());
    }
}