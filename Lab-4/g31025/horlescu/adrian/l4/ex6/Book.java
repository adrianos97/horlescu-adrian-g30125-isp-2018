package ex6;

public class Book
{
    private String name;
    private Author[] author;
    private double price;
    private int qtyInStock = 0;

    public Book(String name, Author[] author, double price)
    {
        this.name = name;
        this.author = author;
        this.price = price;
    }
    public Book(String name, Author[] author, double price, int qtyInStock)
    {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    public String getName()
    {
        return this.name;
    }
    public Author[] getAuthor()
    {
        return author;
    }
    public double getPrice()
    {
        return this.price;
    }
    public void setPrice(double price)
    {
        this.price = price;
    }

    public int getQtyInStock()
    {
        return this.qtyInStock;
    }
    public void setQtyInStock(int qtyInStock)
    {
        this.qtyInStock = qtyInStock;
    }
    public void printAuthors()
    {
        for(Author a : this.author)
            System.out.println(a.getName());
    }
    public String toString()
    {
        return this.getName()+" by "+ getAuthor().length+" authors";
    }
}
