package ex8;

public class Circle extends Shape
{
    private double radius;
    public Circle()
    {
        radius = 1.0;
    }
    public Circle(double radius, String color, boolean field)
    {
        super(color,field);
        this.radius = radius;
    }
    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return 3.14*radius*radius;
    }
    public void setRadius(double radius)
    {
        this.radius = radius;
    }
    public double getPerimeter()
    {
        return 2*3.14*radius;
    }
    public String toString()
    {
        return "Circle radius "+this.radius+" and color "+getColor();
    }
}
