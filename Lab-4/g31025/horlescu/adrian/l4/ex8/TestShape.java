package ex8;

import org.junit.Test;

import static org.junit.Assert.	assertTrue;
import static org.junit.Assert.	assertEquals;

public class TestShape
{
    @Test
    public void shouldGetColor()
    {
        Shape s = new Shape("black", false);
        assertEquals("black",s.getColor());
    }
    @Test
    public void shouldSetColor()
    {
        Shape s = new Shape("black", false);
        s.setColor("white");
        assertEquals("white",s.getColor());
    }
    @Test
    public void testIsFilled()
    {
        Shape s = new Shape("black", false);
        assertEquals(false,s.isFilled());
    }
    @Test
    public void testSetFilled()
    {
        Shape s = new Shape("black", false);
        s.setFilled(true);
        assertEquals(true,s.isFilled() );
    }
}
