package ex4;

import org.junit.Test;

import static org.junit.Assert.	assertTrue;
import static org.junit.Assert.	assertEquals;

public class TestAuthor
{
    @Test
    public void shouldGetName()
    {
        Author a = new Author("Adrian","adrian@gmail.com","m");
        String n3="Adrian";
        assertTrue(n3.equals(a.getName()));
    }
    @Test
    public void shouldGeEmail()
    {
        Author a = new Author("Adrian","adrian@gmail.com","m");
        String n1="adrian@gmail.com";
        assertTrue(n1.equals(a.getEmail()));
    }
    @Test
    public void shouldGetGender()
    {
        Author a = new Author("Adrian","adrian@gmail.com","m");
        String n2="m";
        assertTrue(n2.equals(a.getGender()));
    }
    @Test
    public void shouldSetEmail()
    {
        Author a = new Author("Adrian","adrian@gmail.com","m");
        String n4="horlescu@gmail.com";
        String n5=a.setEmail("horlescu@gmail.com");
        assertTrue(n4.equals(n5));
    }
    @Test
    public void TestToString()
    {
        Author a = new Author("Adrian","adrian@gmail.com","m");
        String n6="Adrian (m) at adrian@gmail.com";
        String n7=a.toString();
        assertTrue(n6.equals(n7));
    }
}
