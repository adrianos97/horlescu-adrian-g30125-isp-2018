package ex7;

import org.junit.Test;

import static org.junit.Assert.	assertEquals;

public class TestCylinder
{
    @Test
    public void shouldGetHeight()
    {
        Cylinder c = new Cylinder(2.5, 5);
        assertEquals(5,c.getHeight(),1);
    }
    @Test
    public void shouldGetVolume()
    {
        Cylinder c = new Cylinder(2.5, 5);
        assertEquals(98.125,c.getVolume(),1);
    }
    @Test
    public void shouldGetArea()
    {
        Cylinder c = new Cylinder(2.5, 5);
        System.out.println(c.getArea());
        assertEquals(117.75,c.getArea(),1);
    }
}
