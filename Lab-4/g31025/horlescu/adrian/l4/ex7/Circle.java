package ex7;

public class Circle
{
    private double radius;
    private String color;
    public Circle()
    {
        radius = 1.0;
        color = "red";
    }
    public Circle(double radius)
    {
        this.radius = radius;
        color = "red";
    }
    public double getRadius()
    {
        return radius;
    }
    public double getArea()
    {
        return 3.14*radius*radius;
    }
    public String toString()
    {
        return "Circle radius "+this.radius+" and color "+this.color;
    }
}
