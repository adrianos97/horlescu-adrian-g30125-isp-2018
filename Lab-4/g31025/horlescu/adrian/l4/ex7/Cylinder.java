package ex7;

public class Cylinder extends Circle
{
    private double height;

    public Cylinder()
    {
        this.height = 1.0;
    }
    public Cylinder(double radius)
    {
        super(radius);
        this.height = 1.0;
    }
    public Cylinder(double radius, double height)
    {
        super(radius);
        this.height = height;
    }
    public double getHeight()
    {
        return this.height;
    }
    public double getVolume()
    {
        return 3.14*getRadius()*getRadius()*this.height;
    }
    @Override
    public double getArea()
    {
        return 2*3.14*getRadius()*this.height + 2*super.getArea();
    }
}
