package g31025.horlescu.adrian.l2;

import java.util.Random;
import java.util.Scanner;

//GUESS THE NUMBER
public class e7
{
    public static void main(String[] args)
    {
        Random r = new Random();
        int x = r.nextInt(20);
        //System.out.println(x);
        int x1,x2,x3;

        Scanner numar = new Scanner(System.in);

        System.out.println("First try, guess the number: ");
        x1 = numar.nextInt();
        if(x1 == x)
        {
            System.out.println("You won!!!");
            System.exit(1);
        }
        if(x1 > x)
        {
            System.out.println("Try a smaller number.");
        }
        else
            System.out.println("Try a bigger number.");

        System.out.println("Second try, guess the number: ");
        x2 = numar.nextInt();
        if(x2 == x)
        {
            System.out.println("You won!!!");
            System.exit(1);
        }
        if(x2 > x)
        {
            System.out.println("Try a smaller number.");
        }
        else
            System.out.println("Try a bigger number.");

        System.out.println("Third try, guess the number: ");
        x3 = numar.nextInt();
        if(x3 == x)
        {
            System.out.println("You won!!!");
            System.exit(1);
        }
        else
            System.out.println("You lost :( , the number was "+x);
    }
}