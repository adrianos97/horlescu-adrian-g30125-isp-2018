package g31025.horlescu.adrian.l2;

import java.util.Scanner;

public class e6
{
    static void nerecursiv(int N)
    {
        int produs=1;
        for(int i=1;i<=N;i++)
        {
            produs=produs*i;
        }
        System.out.println("Rezultatul nerecursiv este: "+produs);
    }
    static int recursiv(int N)
    {
        int produs=1;

        if(N==0 || N==1)
            return 1;

        produs = recursiv(N-1) * N;
        return produs;
    }
    public static void main(String[] args)
    {
        Scanner numar = new Scanner(System.in);
        int N = numar.nextInt();
        int p;
        nerecursiv(N);
        p=recursiv(N);
        System.out.println("Rezultatul recursiv este: "+p);

    }
}
