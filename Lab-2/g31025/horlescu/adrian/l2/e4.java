package g31025.horlescu.adrian.l2;

import java.util.Scanner;
import java.util.Random;

public class e4
{
    public static void main(String[] args)
    {
        Scanner numar_elemente = new Scanner(System.in);
        int n = numar_elemente.nextInt();
        int [] vector;

        vector = new int[n];

        Random r = new Random();
        for(int i=0;i<n;i++)
        {
            vector[i]=r.nextInt();
            System.out.println("Elementul nr."+i+" din vector este: "+vector[i]);
        }
        int max=vector[0];
        for(int j=1;j<n;j++)
        {
            if(vector[j]>vector[j-1])
                max = vector[j];
        }
        System.out.println("Cel mai mare element al vectorului este: "+max);
    }
}
