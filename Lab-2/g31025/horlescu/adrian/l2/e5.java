package g31025.horlescu.adrian.l2;

import java.util.Random;
import java.util.Scanner;

public class e5
{
    static void bubbleSort(int[] arr)
    {
        int n = arr.length;
        int temp = 0;
        for (int i = 0; i < n; i++)
        {
            for (int j = 1; j < (n - i); j++)
            {
                if (arr[j - 1] > arr[j])
                {
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }

            }
        }
    }
        public static void main (String[]args)
        {
            int n = 10;
            int[] vector;

            vector = new int[n];

            Random r = new Random();
            for (int i = 0; i < n; i++) {
                vector[i] = r.nextInt();
                System.out.println("Elementul nr." + i + " din vector este: " + vector[i]);
            }
            bubbleSort(vector);
            for (int j = 0; j < vector.length; j++) {
                System.out.print(vector[j] + " ");
            }

        }

}
