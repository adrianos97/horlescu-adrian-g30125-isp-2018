package ex4;

public class Definition
{
    String definition;

    public Definition(String definition)
    {
        this.definition = definition;
    }

    public String getDefinition(){
        return this.definition;
    }
}
