package ex1;

import java.util.*;
public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public String getOwner() {
        return this.owner;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object b) {
        if(b instanceof BankAccount){
            BankAccount bank = (BankAccount) b;
            return (owner == ((BankAccount) b).owner && balance == ((BankAccount) b).balance);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return owner.hashCode()+(int)balance;
    }

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Adri", 1000);
        BankAccount b2 = new BankAccount("Adri", 1000);
        if (b1.equals(b2)) {
            System.out.println("They are equal");
        } else {
            System.out.println("They are not equal");
        }
        BankAccount b3 = new BankAccount("Ovidiu", 1000);
        BankAccount b4 = new BankAccount("Mike", 600);
        b3.deposit(400);
        if (b3.equals(b4)) {
            System.out.println("They are equal");
        } else {
            System.out.println("They are not equal");
        }
        System.out.println(b2.hashCode() == b1.hashCode());
    }
}
