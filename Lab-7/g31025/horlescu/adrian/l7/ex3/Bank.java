package ex3;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Bank
{
    TreeSet<BankAccount> Accounts = new TreeSet<>();
    public void addAccount(String owner, double balance)
    {
        Accounts.add(new BankAccount(owner,balance));
    }


    public void printAccounts()
    {
        Comparator<BankAccount> byBalance=Comparator.comparingDouble(BankAccount::getBalance);
        Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byBalance);

        TreeSet<BankAccount> sort = new TreeSet<>();
        sort=Accounts.stream().collect(Collectors.toCollection(suplier));
        for(BankAccount b: sort)
            System.out.println(b.getBalance()+" "+b.getOwner());
    }


    public void printAccounts(double minBalance, double maxBalance)
    {
        for(BankAccount b:Accounts)
            if(b.getBalance()>minBalance && b.getBalance()<maxBalance)
                System.out.println(b.getBalance()+" "+b.getOwner());
    }


  public TreeSet<BankAccount> getAllAcounts(){
      Comparator<BankAccount> byName=Comparator.comparing(BankAccount::getOwner);
      Supplier<TreeSet<BankAccount>> suplier=()->new TreeSet<BankAccount>(byName);

      return Accounts.stream().collect(Collectors.toCollection(suplier));



  }





}