package ex3;

public class Program {

    public static void main(String[] args){
        Bank bank = new Bank();
        bank.addAccount("Sefu",100);
        bank.addAccount("Teo",50);
        bank.addAccount("Teo",10);
        bank.addAccount("Robi",70);
        bank.addAccount("RobiSefu",20);
        bank.addAccount("SefulRobi",4);
        bank.addAccount("RobiSef",80);


        bank.printAccounts();
        System.out.println();
        bank.printAccounts(49,100);

        System.out.println();
       for(BankAccount b : bank.getAllAcounts()){
           System.out.println(b.getOwner() + " " + b.getBalance());
       }
    }
}
