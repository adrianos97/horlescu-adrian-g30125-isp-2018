package ex2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Bank
{
    ArrayList<BankAccount> Accounts = new ArrayList<BankAccount>();
    public void addAccount(String owner, double balance)
    {
        Accounts.add(new BankAccount(owner,balance));
    }
    public void printAccounts()
    {
        Collections.sort(Accounts);
        for(BankAccount b:Accounts)
            System.out.println(b.getBalance()+" "+b.getOwner());
    }
    public void printAccounts(double minBalance, double maxBalance)
    {
        for(BankAccount b:Accounts)
            if(b.getBalance()>minBalance && b.getBalance()<maxBalance)
                System.out.println(b.getBalance()+" "+b.getOwner());
    }
    public BankAccount getAccount(String owner)
    {
        return null;
    }

    public ArrayList<BankAccount> getAllAccounts() {
        return Accounts;
    }



 /*   public static void main(String[] args) {
        Bank bank = new Bank();


        bank.addAccount("adri",1000);
        bank.addAccount("teo",300);
        bank.addAccount("coste",400);
        bank.addAccount("geo",7000);
        bank.addAccount("coste",650);
        bank.addAccount("robi",900);
        bank.printAccounts();
        System.out.println(" ");
        bank.printAccounts(200,1000);
        ArrayList<BankAccount> bankAccounts = bank.getAllAccounts();
        Collections.sort(bankAccounts,BankAccount.ownerComparator);
        System.out.println(" ");
        for(BankAccount b:bankAccounts)
            System.out.println(b.getBalance()+" "+b.getOwner());
    }
*/
}
