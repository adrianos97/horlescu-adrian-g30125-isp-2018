package ex2;

import java.util.*;
public class BankAccount implements Comparable<BankAccount>
{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public String getOwner() {
        return this.owner;
    }

    public double getBalance() {
        return balance;
    }



    @Override
    public boolean equals(Object b) {
        if(b instanceof BankAccount){
            BankAccount bank = (BankAccount) b;
            return (owner == ((BankAccount) b).owner && balance == ((BankAccount) b).balance);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return owner.hashCode()+(int)balance;
    }

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Adri", 1000);
        BankAccount b2 = new BankAccount("Adri", 1000);
        if (b1.equals(b2)) {
            System.out.println("They are equal");
        } else {
            System.out.println("They are not equal");
        }
        BankAccount b3 = new BankAccount("Ovidiu", 1000);
        BankAccount b4 = new BankAccount("Mike", 600);
        b3.deposit(400);
        if (b3.equals(b4)) {
            System.out.println("They are equal");
        } else {
            System.out.println("They are not equal");
        }
        System.out.println(b1.compareTo(b4));
    }

    public static Comparator<BankAccount> ownerComparator
            = new Comparator<BankAccount>() {

        public int compare(BankAccount b1, BankAccount b2) {

            return b1.getOwner().compareTo(b2.getOwner());

        }

    };

    @Override
    public int compareTo(BankAccount o) {
        return (int)(this.balance-o.getBalance());
    }
}
