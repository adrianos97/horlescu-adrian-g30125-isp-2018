package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class ButtonCounter extends JFrame implements ActionListener{

    JButton bCounter;
    JTextField tCounter;
    int count = 0;

    ButtonCounter()
    {
        setTitle("Button Counter // Ex 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500,500);
        setVisible(true);
    }
    public void init()
    {
        this.setLayout(null);
        int width = 180; int height = 20;

        bCounter = new JButton("Press to Count");
        bCounter.setBounds(150,150,width,height);

        bCounter.addActionListener((ActionListener) this);

        tCounter = new JTextField(count + "");
        tCounter.setBounds(150,180,width,height);

        add(bCounter); add(tCounter);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        ++count;
        tCounter.setText("Button pressed "+ count + " times");
    }

    public static void main(String[] args) {
        ButtonCounter a = new ButtonCounter();
    }
}
