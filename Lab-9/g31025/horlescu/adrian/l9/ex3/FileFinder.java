package ex3;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.io.IOException;
import java.awt.*;

public class FileFinder extends JFrame{
    private JLabel file;
    private JTextField fileName;
    private JTextArea content;
    private JButton click;
    FileFinder()
    {
        this.setTitle("File_Finder");
        this.setSize(500,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try{
            init();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
        this.setVisible(true);
    }
    public void init () throws IOException
    {
        this.setLayout(null);
        int width=80;int height = 20;
        file=new JLabel("File Name:");
        file.setBounds(10, 50, width, height);
        fileName=new JTextField("");
        fileName.setBounds(75,50,width, height);
        content=new JTextArea("");
        content.setBounds(20,100,300,300);
        click=new JButton("Search");
        click.setBounds(170,50,width,height);

        click.addActionListener(new ActionListener(){


            public void actionPerformed(ActionEvent e) {
                if(e.getSource()==click)
                {
                    String file;
                    file=FileFinder.this.fileName.getText();
                    File f=new File(file);
                    if(!f.exists())
                        FileFinder.this.content.append("File Not Found!");
                    else
                    {
                        try{
                            DataInputStream dis=new DataInputStream(new FileInputStream(f));
                            String info;
                            info=dis.readLine();
                            while(info!=null)
                            {
                                FileFinder.this.content.append(info+"\n");
                                info=dis.readLine();
                            }
                        }catch(IOException ev)
                        {
                            ev.printStackTrace();
                        }

                    }

                }

            }

        });
        add(file);
        add(fileName);
        add(content);
        add(click);



    }
    public static void main(String[] args)
    {
        new FileFinder();
    }
}