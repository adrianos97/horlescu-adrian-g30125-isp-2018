import becker.robots.*;

public class e5
{
    public static void main(String[] args)
    {
        City ny = new City();
        Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
        Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
        Wall blockAve2 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall blockAve3 = new Wall(ny, 1, 2, Direction.NORTH);
        Wall blockAve4 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall blockAve5 = new Wall(ny, 1, 2, Direction.EAST);
        Wall blockAve6 = new Wall(ny, 1, 2, Direction.SOUTH);
        Robot crry = new Robot(ny, 1, 2, Direction.SOUTH);
        Thing newspaper = new Thing(ny, 2, 2);

        crry.turnLeft();
        crry.turnLeft();
        crry.turnLeft();
        crry.move();
        crry.turnLeft();
        crry.move();
        crry.turnLeft();
        crry.move();
        crry.pickThing();
        crry.turnLeft();
        crry.turnLeft();
        crry.move();
        crry.turnLeft();
        crry.turnLeft();
        crry.turnLeft();
        crry.move();
        crry.turnLeft();
        crry.turnLeft();
        crry.turnLeft();
        crry.move();
        crry.putThing();
        crry.turnLeft();
        crry.turnLeft();
        crry.turnLeft();
    }
}
