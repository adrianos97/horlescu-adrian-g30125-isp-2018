package g31025.horlescu.adrian.l3;

import java.util.*;
public class MyPoint
{
    private int x,y;
   public MyPoint()
    {
        this.x = 0;
        this.y = 0;
    }
    public MyPoint(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    int getX()
    {
        return this.x;
    }
    int getY()
    {
        return this.y;
    }
    void setXY(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    String ToString()
    {
        return "("+this.x+","+this.y+")";
    }
    double distance(int x, int y)
    {
        double sum1=0, sum2=0;
        sum1 = Math.pow(x-this.x,2) + Math.pow(y-this.y,2);
        sum2 = Math.sqrt(sum1);
        return sum2;
    }
    double distance(MyPoint anotherPoint)
    {
        double sum1=0,sum2=0;
        sum2=Math.pow(anotherPoint.getX()-this.x, 2)+Math.pow(anotherPoint.getY()-this.y,2);
        sum1=Math.sqrt(sum2);
        return sum1;
    }
}
