import becker.robots.*;

public class e3
{
    public static void main(String[] args)
    {
        // Set up the initial situation
        City prague = new City();
        Robot adrian = new Robot(prague, 1, 1, Direction.NORTH);

        adrian.move();  //moves north 5 times
        adrian.move();
        adrian.move();
        adrian.move();
        adrian.move();
        adrian.turnLeft();      // start turning around as two turn lefts
        adrian.turnLeft();      // finish turning around
        adrian.move();  //moves south 5 times
        adrian.move();
        adrian.move();
        adrian.move();
        adrian.move();
    }
}
