package ex5;

public class Pyramid {

    Brick brick;
    int nr;

    public Pyramid(Brick brick, int nr) {
        this.brick = brick;
        this.nr = nr;
    }


    public void buildPyramid(){
        int level = 1;
        int space = nr;
        while (nr > 0){
            if ((level+1 <= nr-level) || (level == nr)){
                for (int i = 0; i<space; i++)
                    System.out.print(" ");
                for(int i=0; i<level; i++){
                    System.out.print("¤ ");
                }
                space--;
                nr-=level;
                level++;
                System.out.println();
            }
            else {
                space=space-(nr-level);
                for (int i = 0; i<space; i++)
                    System.out.print(" ");
                for (int i = 0; i < nr; i++) {
                    System.out.print("¤ ");
                }
                nr = 0;
            }
        }
    }
}