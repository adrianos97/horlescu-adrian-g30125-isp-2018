package ex1;

import java.awt.*;


public abstract class Shape {

    private Color color;
    private int x,y;
    String id;
    boolean filled;

    public Shape(String id,Color color,int x, int y, boolean filled) {
        this.id = id;
        this.color = color;
        this.x  = x;
        this.y = y;
        this.filled = filled;
    }

    public Color getColor() {
        return color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getId() {
        return id;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}