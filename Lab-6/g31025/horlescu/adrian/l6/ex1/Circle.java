package ex1;

import java.awt.*;

public class Circle extends Shape {

    private int radius;

    public Circle(String id, Color color, int x, int y, boolean filled, int radius) {
        super(id, color, x, y, filled);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle " + this.radius + " " + getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(), getY(), radius, radius);
        if (isFilled())
            g.fillOval(getX(), getY(), radius, radius);
    }
}