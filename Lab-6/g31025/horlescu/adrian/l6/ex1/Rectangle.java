package ex1;

import java.awt.*;

public class Rectangle extends Shape{

    private int width, height;

    public Rectangle(String id,Color color, int x, int y, boolean filled, int width, int height) {
        super(id,color,x,y,filled);
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+width+"by "+height+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),getWidth(),getHeight());
        if(isFilled())
            g.fillRect(getX(),getY(),getWidth(),getHeight());
    }
}