package ex3;

import java.awt.*;


public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle("01",Color.RED,50,200,true, 90);
        b1.addShape(s1);
        Shape s2 = new Rectangle("02",Color.BLUE,50,100,true, 100,50);
        b1.addShape(s2);
        Shape s3 = new Rectangle("03",Color.orange,100,50,false, 50,50);
        b1.addShape(s3);
        b1.deleteById("01");
    }
}