package ex3;

import java.awt.*;

public class Circle implements Shape{
    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean filled;
    private int radius;
    Graphics g;

    public Circle(String id, Color color, int x, int y, boolean filled, int radius) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.filled = filled;
        this.radius = radius;
    }

    public String getId() {
        return id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isFilled() {
        return filled;
    }

    public void draw(Graphics g) {
        System.out.println("Drawing a circle " + this.radius + " " + getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(), getY(), radius, radius);
        if (isFilled())
            g.fillOval(getX(), getY(), radius, radius);
    }

}
