package ex3;

import java.awt.*;

public interface Shape
{
    public void draw(Graphics g);
    public String getId();
}
