package ex3;

import java.awt.*;

public class Rectangle implements Shape{

    private Color color;
    private int x;
    private int y;
    private String id;
    private Boolean filled;
    private int width,height;
    Graphics g;


    public Rectangle(String id,Color color, int x, int y, boolean filled, int width, int height) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.filled = filled;
        this.width = width;
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    public boolean isFilled() {
        return filled;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+width+"by "+height+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),getWidth(),getHeight());
        if(isFilled())
            g.fillRect(getX(),getY(),getWidth(),getHeight());
    }
}
