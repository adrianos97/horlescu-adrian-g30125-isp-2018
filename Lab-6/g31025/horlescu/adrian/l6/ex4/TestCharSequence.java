package ex4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCharSequence {

    @Test
    public void ShouldPrintTheLengthOfTheArray() {
        char[] seq= new char[]{'d', 'f', 'g', 'h'};
        CharSequenceImplement string = new CharSequenceImplement(seq);
        assertEquals(4, string.length());
    }

    @Test
    public void ShouldGetTheChar() {
        char[] seq= new char[]{'d', 'f', 'g', 'h'};
        CharSequenceImplement string = new CharSequenceImplement(seq);
        assertEquals('f',string.charAt(1));
    }

    @Test
    public void ShouldPrintTheSubsequence() {
        char[] seq= new char[]{'d', 'f', 'g', 'h'};
        CharSequenceImplement string = new CharSequenceImplement(seq);
        assertEquals("df", string.subSequence(0,2).toString());
    }
}