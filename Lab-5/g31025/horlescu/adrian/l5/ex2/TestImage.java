package ex2;

import org.junit.Test;

public class TestImage
{
    @Test
    public void testImage()
    {
        Image[] images = new Image[3];
        images[0] = new RealImage("laborator");
        images[1] = new ProxyImage("examen");
        images[2] = new RotatedImage("picat");

        images[0].display();
        images[1].display();
        images[2].display();
    }
}
