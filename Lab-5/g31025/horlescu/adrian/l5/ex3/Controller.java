package ex3;


public class Controller {

    TemperatureSensor tempSensor;
    LightSensor lightSensor;

    public Controller (TemperatureSensor tempSensor, LightSensor lightSensor){
        this.tempSensor = tempSensor;
        this.lightSensor = lightSensor;
    }


    public void control() throws InterruptedException {
        System.out.println(tempSensor.readValue());
        Thread.sleep(1000);
        System.out.println(lightSensor.readValue());
     }
}
