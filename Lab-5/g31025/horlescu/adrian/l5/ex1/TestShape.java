package ex1;

import org.junit.Test;

import static org.junit.Assert.	assertTrue;
import static org.junit.Assert.	assertEquals;;

public class TestShape
{
    @Test
    public void shouldGetArea()
    {
        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle("red", true, 10);
        shapes[1] = new Rectangle(5,10,"blue",true);
        shapes[2] = new Square(10, "white", true);

        assertEquals(300,shapes[0].getArea(),1);
        assertEquals(50,shapes[1].getArea(),1);
        assertEquals(100,shapes[2].getArea(),1);
    }
    @Test
    public void shouldGetPerimeter()
    {
        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle("red", true, 10);
        shapes[1] = new Rectangle(5,10,"blue",true);
        shapes[2] = new Square(10, "white", true);

        assertEquals(60,shapes[0].getPerimeter(),1);
        assertEquals(30,shapes[1].getPerimeter(),1);
        assertEquals(40,shapes[2].getPerimeter(),1);
    }
}
